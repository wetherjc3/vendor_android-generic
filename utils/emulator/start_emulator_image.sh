#!/bin/sh
vendor_path="android-generic"

# Device type selection
PS3='Which device type do you plan on building?: '
options=("nexus_5"
		 "nexus_6"
		 "nexus_7"
		 "nexus_10"
		 "oneplus_6"
		 "pixel_xl"
		 "pixel_2_xl")
select opt in "${options[@]}"
do
	case $opt in
		"nexus_5")
			echo "you chose choice $REPLY which is $opt"
			ag_skin=$opt
			break
			;;
		"nexus_6")
			echo "you chose choice $REPLY which is $opt"
			ag_skin=$opt
			break
			;;
		"nexus_7")
			echo "you chose choice $REPLY which is $opt"
			ag_skin=$opt
			break
			;;
		"nexus_10")
			echo "you chose choice $REPLY which is $opt"
			ag_skin=$opt
			break
			;;
		"oneplus_6")
			echo "you chose choice $REPLY which is $opt"
			ag_skin=$opt
			break
			;;
		"pixel_xl")
			echo "you chose choice $REPLY which is $opt"
			ag_skin=$opt
			break
			;;
		"pixel_2_xl")
			echo "you chose choice $REPLY which is $opt"
			ag_skin=$opt
			break
			;;
		*) echo "invalid option $REPLY";;
	esac
done

export ANDROID_BUILD_TOP=`pwd`
export ANDROID_PRODUCT_OUT=`pwd`

$HOME/Android/Sdk/emulator/emulator  -verbose -skindir $ANDROID_BUILD_TOP/skins/ -skin $ag_skin -gpu host -writable-system -qemu -cpu qemu64 -append "vbmeta"
