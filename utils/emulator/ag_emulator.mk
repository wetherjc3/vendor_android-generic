PRODUCT_COPY_FILES += \
    vendor/android-generic/utils/emulator/fstab.ranchu:vendor/etc/fstab.ranchu

$(call inherit-product, build/target/product/sdk_x86.mk)

$(call inherit-product, vendor/android-generic/config/gsm.mk)

$(call inherit-product, vendor/android-generic/config/common.mk)

$(call inherit-product, vendor/android-generic/utils/emulator/common.mk)

# Get proprietary files if any exists
$(call inherit-product, vendor/boringdroid/boringdroid.mk)

# Inherit vendors
$(call inherit-product,vendor/android-generic/config/vendors.mk)

# Override product naming for android-generic
PRODUCT_NAME := ag_emulator

DEVICE_PACKAGE_OVERLAYS += vendor/android-generic/utils/emulator/overlay

ALLOW_MISSING_DEPENDENCIES := true 
TARGET_INCLUDE_WIFI_EXT := false
TARGET_FLATTEN_APEX := true

