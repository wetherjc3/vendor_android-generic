<img src="https://i.imgur.com/HrTFRpm.png">

# Android-Generic (x86) 1.1 - Android for PCs & GSI devices

### What is Android-Generic Project?:

Android-Generic is a collection of scripts, manifests & patches that allow for rapid prototyping of Android projects based off AOSP to produce generic images for Android GSI or Linux PC hardware. 

### Website:

[Website](https://android-generic.github.io)

### Info:

For a more in-depth description of the goals and progressions leading up to this project, please read: 
[This Blog Post](https://blog.blissroms.com/2020/06/26/lets-try-and-change-the-game/)

### Vision:

To fill you all in on the grand scheme of things with Android-Generic, the plan come in stages. 

* Stage 1 is laying the groundwork for all ROMs to build both PC and GSI builds by supplying them with a standard set of minimal patches. Thanks to Android-x86  & PHH-Treble, thats done. now we collect them and make them easy to apply for everyome. **DONE**

* Stage 2 is collecting variations of the conflicts those standard patches could potentially have and simplifying the process as we go. This is where all the Android ROMs come in ;) **STARTED**

* Stage 3 is to use the variations of conflict resolutions from each ROM folder to further automate the process and have vendor patches be automatically figured out by trying each resolution until one sticks. **STARTED** 

* Stage 4 is to continue to automate elements of the process until things are almost too easy **STARTED**

-----------

### Development Chats:

Telegram:

[GSI/Treble](https://t.me/androidgenericgsi)
    
[PC/x86/x86_64](https://t.me/androidgenericpc)


### Resources:

Images:
[Android-Generic Logos](https://bit.ly/2BQc5Vi)

Test Builds:
[Android-Generic Test Builds](https://www.blissos.org/#ag-downloads)

Source & Troubleshooting Documentation:
[Android-x86 Documentation](https://www.android-x86.org/documentation.html)

[Bliss OS Docs](https://docs.blissroms.com/Bliss%20OS/)

[Bliss Wiki](https://wiki.blissroms.com/index.php?title=BlissWiki)

-----------
# Table of Contents

------
[Getting Started](#getting-started) 
-----

[Prerequisites](#prerequisites)

[What you need to build with](#what-you-need-to-build-with)

[Verify your ROM is ready](#verify-your-rom-is-ready)

[Getting The Repository](#getting-the-repository)

------
[Instructions For Setting Up Your Environment For PC Builds](#instructions-for-setting-up-your-environment-for-pc-builds-1)
------

[Proprietary PC Files](#proprietary-files)

[Building Android For PC Explained](#building-android-for-pc)

[Getting Your PC Build Going](#now-for-the-fun-stuff-getting-your-build-going)

[Building for Treble Explained](#building-for-treble-devices)

[Getting your Treble Build Going](#now-for-the-fun-stuff-getting-your-treble-build-going)

------
[Adding your vendor to Android-Generic](#adding-your-vendor-to-android-generic-1)
------

[Introduction to adding your vendor support](#introduction-to-adding-your-vendor-support)

[Updating the PC Scripts](#updating-the-pc-scripts)

[Updating the PC Manifests](#updating-the-pc-manifests)

[Using manifest patches](#using-manifest-patches)

[Updating the Treble Scripts](#updating-the-treble-scripts)

[Updating the Treble Manifests](#updating-the-treble-manifests)

[Resolving Patch Conflicts](#resolving-patch-conflicts)
-------
[Adding Custom Vendor Patchesets](#adding-custom-vendor-patchesets-1)
-------
[Updating the Script Repo](#updating-the-script-repo-1)
-------

------
[Credits](#credits-1)
------

# Getting Started

This portion of the instructions is for both PC & GSI builds. 

### Prerequisites:

You will need to have synced a ROM prior to adding this to your build envirnment. 

For PC builds (so far):
	
	- BlissROM
	- AOSP
	- Tesla 
	- WIP - Tipsy
	- WIP - Lineage OS
	- WIP - Validus
	- Carbon
	- Pixel Experience
	- Dirty Unicorns
	- WIP - RR
	- WIP - GZOSP

For GSI builds (so far):
	
	- BlissROM
	- AOSP
	- WIP - CRDroid
	- WIP - Lineage OS

Please make sure you're well versed in building AOSP: [AOSP building instructions](http://source.android.com/source/index.html) before proceeding.

### What you need to build with 

[android-generic](https://gitlab.com/android-generic/vendor_android-generic)

    Latest Ubuntu LTS Releases https://www.ubuntu.com/download/server
    Decent CPU (Dual Core or better for a faster performance)
    8GB RAM (16GB for Virtual Machine)
    250GB Hard Drive (about 170GB for the Repo and then building space needed)
  
#### Installing Java 8

    sudo apt-get update && upgrade
    sudo apt-get install openjdk-8-jdk-headless
    update-alternatives --config java  (make sure Java 8 is selected)
    update-alternatives --config javac (make sure Java 8 is selected)
    
#### Grabbing Dependencies

    $ sudo apt-get install git-core gnupg flex bison maven gperf build-essential zip curl zlib1g-dev gcc-multilib g++-multilib libc6-dev-i386  lib32ncurses5-dev x11proto-core-dev libx11-dev lib32z-dev ccache libgl1-mesa-dev libxml2-utils xsltproc unzip squashfs-tools python-mako libssl-dev ninja-build lunzip syslinux syslinux-utils gettext genisoimage gettext bc xorriso libncurses5 xmlstarlet build-essential git imagemagick lib32readline-dev lib32z1-dev liblz4-tool libncurses5-dev libsdl1.2-dev libxml2 lzop pngcrush rsync schedtool python-enum34 python3-mako libelf-dev xmlstarlet
    
#### Setting Up Cache

Add these lines to your `.bashrc` file:

    ccache -F 0 && ccache -M 0 
	export CCACHE_DIR=$HOME/.ccache
	export USE_CCACHE=1 
	export CCACHE_TEMPDIR=/tmp 
	export EXPERIMENTAL_USE_JAVA8=true
	
#### Installing and Initializing Repo

Follow the instructions on the Android Source Build site to install `repo` and setting up your distribution's manifest.

    https://source.android.com/setup/build/downloading#installing-repo

### Verify your ROM is ready

Before we can get our build on, we will need to make sure we have the ROM of your choice setup first. Here are a couple examples of that using Bliss & AOSP 

#### For Bliss:

Repo initialization :

	$ repo init -u https://github.com/BlissRoms/platform_manifest.git -b q

sync repo :

	$ repo sync -c -j$(nproc --all) --force-sync --no-clone-bundle --no-tags


#### For aosp:

Repo initialization :

	$ repo init -u https://android.googlesource.com/platform/manifest -b android-10.0.0_r37

sync repo :

	$ repo sync -c -j$(nproc --all) --force-sync --no-clone-bundle --no-tags

#### For YOUR ROM

Head to [Adding your vendor to Android-Generic](#adding-your-vendor-to-android-generic-1) and help us help you ;)

### Getting The Repository

Starting from your project folder, follow each step as follows:

Clone Repo :
    
	$ git clone https://gitlab.com/android-generic/vendor_android-generic vendor/android-generic

Setup Build Envirnment :

    $ . build/envsetup.sh

From this point, you need to choose your own adventure. 

For PC (x86/x86_64) builds, [continue here](#instructions-for-setting-up-your-environment-for-pc-builds-1)

For GSI (Treble) builds, [continue here](#instructions-for-setting-up-your-environment-for-treble-builds)
    
# Instructions For Setting Up Your Environment For PC Builds

To start, you must first use the -s (--sync) flag, then on following builds, it is not needed. 
You can do this using the build script.

First, sync using the build script to add in all the Android-Generic additions :

	$ build-x86 -s

At this point it will ask you what your device target is (Atom, Generic, Vulkan, etc) 

    Just answer with 1,2,3,etc. 
    
This will then sync all the ROM sources + Android-Generic additions, and patch afterwards. 
You might want to copy the entire patching output to a notepad in order to determine what actually had a conflict once the process was finished (since there are a few steps there, it can get confusing)

If you do have conflicts, skip to the [Resolving Patch Conflicts](#resolving-patch-conflicts) section to find out what to do next before moving forward. 

### Proprietary Files

(Houdini & Widevine from Chrome OS)

Initial extraction of the proprietary files from Google are also needed on the first build. 
Please note that this process will need to be run for x86 & x86_64 builds seperately (some cleanup may be needed). 

We are able to use the -r (--proprietary) flag for that. This step needs to be done once per device setup (x86 or x86_64) and we have it working on its own because
the image mounting process requires root permissions, so keep a look out for it asking for your root password. 

The proprietary steps is for both Chrome OS files as well as pulling in the latest FOSS apps. Each will run back to back with no break

Grabbing Proprietary Files (make sure you add your target here; android_x86 or android_x86_64) :

    $ build-x86 -r android_x86_64-userdebug foss

### Building Android For PC

PC builds (x86) explained:
	  
This script will allow you to build an x86 based .ISO for PCs as well as help you with a few other things. Please see details below

#### Usage :

	$ build-x86 options buildVariants extraOptions addons

#### Options : 

	-c | --clean : Does make clean && make clobber and resets the device tree
	-s | --sync: Repo syncs the rom (clears out patches), then reapplies patches to needed repos
	-p | --patch: Just applies patches to needed repos
	-r | --proprietary: build needed items from proprietary vendor (non-public)
    -o | --oldproprietary: build needed items from old proprietary vendor/bliss_priv for ChromeOS houdini & widevine
    -d | --desktopmode: build with desktop mode enabled by default
    -i | --iptsdrivers: build with Intel IPTS kernel modules added
    -e | --efi_img: build as an EFI .img file (depends on branch of bootable/newinstaller)
    -u | --subsync: forces all submodules for ax86-nb-qemu to init & sync
	-k | --kernel: build with specific kernel branch
    -b | --backup: Generate a manifest backup with revisions included. (Helpful when ROMs update miltiple times a week)
    -n | --vendor-setup __vendorname__ : Creates all the required folders & files to start building for your ROM.

#### BuildVariants :

	android_x86-user : Make user build
	android_x86-userdebug |: Make userdebug build
	android_x86-eng : Make eng build
	android_x86_64-user : Make user build
	android_x86_64-userdebug |: Make userdebug build
	android_x86_64-eng : Make eng build

#### ExtraOptions : Defaults to None

	foss : packages microG & FDroid with the build
	fdroid : packages custom FDroid with the build (requires private sources)
	go : packages Gapps Go with the build (when go vendor is synced)
	gapps : packages OpenGapps with the build (when OpenGapps vendor sources are synced)
	gms : packages GMS with the build (requires private repo access)
	none : force all extraOption flags to false. (Drfault Option)

#### Addons : Requires "--proprietary" build to have run at least once. Defaults to None

	croshoudini : Include libhoudini from Chrome OS 
	croswidevine : Include widevine from Chrome OS
	crosboth : Include both libhoudini and widevine from Chrome OS
    crospriv : Include both libhoudini and widevine from old vendor/bliss_priv method for Chrome OS 
	crosnone : Do not include any of them. (Default Option)
    x86nb : Use ax86-NB Source: https://github.com/goffioul/ax86-nb-qemu (currently only working with x86 (32bit) builds)

## Now For The Fun Stuff, Getting Your Build Going

Once you have gone through all the above steps (syncing, patching, proprietary files). You are ready to start your first build of Android for PC. Use the options above to specify what kind of build you want. 

(to build the userdebug version for x86_64 CPUs with FDroid & microG, along with widevine from ChromeOS included)

	$ build-x86 android_x86_64-userdebug foss croswidevine 

(to build the userdebug version for x86_64 CPUs with Play Store and widevine from ChromeOS included)

	$ build-x86 android_x86_64-userdebug gms croswidevine 

(to build the userdebug version for x86 CPUs with Play Store and ax86-nativebridge included, and choose a different kernel branch upon build-time)

	$ build-x86 android_x86-userdebug -k gms x86nb 


# Instructions For Setting Up Your Environment For Treble Builds

To start, you must first use the -s (--sync) flag, then on following builds, it is not needed. 
You can do this using the build script.

First, sync using the build script to add in all the Android-Generic additions :

	$ build-treble -s

From here, it will sync first, then go through the three rounds of patches (vendor_prepatch, treble patches, and another round of vendor patches)

### Building for Treble devices

Treble builds (GSI) explained:
	  
This script will allow you to build a GSI based image with your ROM as well as help you with a few other things. Please see details below

#### Usage :

	$ build-treble options buildVariants

#### Options : 

	-c | --clean : Does make clean && make clobber and resets the device tree
	-r | --release : Builds a twrp zip of the rom (only for A partition) default creates system.img
	-s | --sync: Repo syncs the rom (clears out patches), then reapplies patches to needed repos
	-p | --patch: Just applies patches to needed repos
    -b | --backup: Generate a manifest backup with revisions included. (Helpful when ROMs update miltiple times a week)
    -n | --vendor-setup __vendorname__ : Creates all the required folders & files to start building

#### BuildVariants :

	arm64_a_stock 	| arm64_ab_stock : Vanilla Rom
    arm64_a_gapps 	| arm64_ab_gapps : Stock Rom with Gapps Built-in
	arm64_a_foss  	| arm64_ab_foss : Stock Rom with Foss
	arm64_a_go 	  	| arm64_ab_go : Stock Rom with Go-Gapps
	arm64_ab_stock 	| arm64_ab_stock : Vanilla Rom
    arm64_ab_gapps 	| arm64_ab_gapps : Stock Rom with Gapps Built-in
	arm64_ab_foss  	| arm64_ab_foss : Stock Rom with Foss
	arm64_ab_go 	| arm64_ab_go : Stock Rom with Go-Gapps
	a64_a_stock 	| arm64_ab_stock : Vanilla Rom
    a64_a_gapps 	| arm64_ab_gapps : Stock Rom with Gapps Built-in
	a64_a_foss  	| arm64_ab_foss : Stock Rom with Foss
	a64_a_go 	  	| arm64_ab_go : Stock Rom with Go-Gapps
	a64_ab_stock 	| arm64_ab_stock : Vanilla Rom
    a64_ab_gapps 	| arm64_ab_gapps : Stock Rom with Gapps Built-in
	a64_ab_foss  	| arm64_ab_foss : Stock Rom with Foss
	a64_ab_go 	  	| arm64_ab_go : Stock Rom with Go-Gapps
	arm_a_stock 	| arm64_ab_stock : Vanilla Rom
    arm_a_gapps 	| arm64_ab_gapps : Stock Rom with Gapps Built-in
	arm_a_foss  	| arm64_ab_foss : Stock Rom with Foss
	arm_ab_go 	  	| arm64_ab_go : Stock Rom with Go-Gapps
	arm_ab_stock 	| arm64_ab_stock : Vanilla Rom
    arm_ab_gapps 	| arm64_ab_gapps : Stock Rom with Gapps Built-in
	arm_ab_foss  	| arm64_ab_foss : Stock Rom with Foss
	arm_ab_go 	  	| arm64_ab_go : Stock Rom with Go-Gapps

## Now For The Fun Stuff, Getting Your Treble Build Going

Once you have gone through all the above steps (syncing, patching). You are ready to start your first build of Android-Generic GSI. Use the options above to specify what kind of build you want. 

(to build the userdebug version for arm64_a with FDroid & microG)

	$ build-treble arm64_a_foss 

(to build the userdebug version for arm_ab with gapps)

	$ build-treble arm_ab_gapps 

(to build the userdebug version for a64_ab with no gapps or FOSS apps)

	$ build-treble a64_ab_stock 
	
	
### Other Treble Build Option

If you want to build all buildVariants back to back, we also have that built in as well. 

#### Options : 

	- gapps - Add Gapps
	- foss - Add FOSS apps
	- stock - Add no extra apps
	- go - Add Go Gapps

#### Build All GSI Variants : 
	
	$ build-all-treble option 

---------
# Adding your vendor to Android-Generic
---------

## Introduction to adding your vendor support

For ROMs not already added to AG, there are a few things that you will need to update in the source before things are ready to patch on your side.
First step is to run the -n | --vendor-setup option:

    $ build-x86 -n __YourVendorName__

Or:

    $ build-x86 --vendor-setup __YourVendorName__

This will create the initial patch and manifest folder entries for you. To know where to look, see below.

PC Patches:

	* Vendor specific prepatches ( vendor/android-generic/patches/google_diff/pc_vendor_prepatches/__YourVendorName__ )
	* Vendor specific manifests ( vendor/android-generic/manifests/android_pc/__YourVendorName__ )
    * Vendor specific patches ( vendor/android-generic/patches/google_diff/pc_vendor_patches/__YourVendorName__/patches )
    * Vendor specific custom_patches ( vendor/android-generic/patches/google_diff/pc_vendor_patches/__YourVendorName__/custom_patches )

GSI Patches:

	* Vendor specific prepatches ( vendor/android-generic/patches/google_diff/treble_vendor_prepatches/__YourVendorName__ )
	* Vendor specific manifests ( vendor/android-generic/manifests/treble/__YourVendorName__ )
    * Vendor specific patches ( vendor/android-generic/patches/google_diff/treble_vendor_patches/__YourVendorName__/patches )
    * Vendor specific custom_patches ( vendor/android-generic/patches/google_diff/treble_vendor_patches/__YourVendorName__/custom_patches )

To break things down a little, let's take things one at a time, but starting with updating the script, then the Manifests and Patches. 

## Vendor specific overlay support

When creating a new vendor profile, sometimes you may need to add specific overlays to the ROM and you don't want to do this with patches. 
For this case, we added support in the vendor_patches folder for that. 

PC Overlays:

	* Vendor specific overlay ( vendor/android-generic/patches/google_diff/pc_vendor_patches/__YourVendorName__/overlay )

GSI Overlays:

	* Vendor specific overlay ( vendor/android-generic/patches/google_diff/treble_vendor_patches/__YourVendorName__/overlay )

To break things down a little, let's take things one at a time, but starting with updating the script, then the Manifests and Patches. 

### Updating the PC Scripts

This is for process documentation only. The vendor setup build options do all this work for us now. 

First step is to add your vendor to the build system. 

    $ nano vendor/android-generic/pc_roms.lst

Then add your vendor name to the bottom of the list: 

    bliss
    lineage
    tesla
    tipsy
    __YourVendorName__

Next script we have to edit is located in the bootable/newinstaller/Android.mk file

    $ nano bootable/newinstaller/Android.mk

Scroll down to the "# Use vendor defined version names" section (about line 129) and add your new ROM_IS_VENDORNAMEHERE to coincide with your vendor's version naming variable. 

    # Use vendor defined version names
    ROM_VENDOR_VERSION := $(TARGET_PRODUCT)-$(shell date +%Y%m%d%H%M)

    ifeq ($(ROM_IS_BLISS),true)
    ROM_VENDOR_VERSION := $(BLISS_VERSION)-$(TARGET_PRODUCT)
    endif

    ifeq ($(ROM_IS_LINEAGE),true)
    ROM_VENDOR_VERSION := $(LINEAGE_VERSION)-$(TARGET_PRODUCT)
    endif

    ifeq ($(ROM_IS_TESLA),true)
    ROM_VENDOR_VERSION := $(TESLA_VERSION)-$(TARGET_PRODUCT)
    endif

    ifeq ($(ROM_IS_TIPSY),true)
    ROM_VENDOR_VERSION := $(TIPSY_VERSION)-$(TARGET_PRODUCT)
    endif

After that, we can get on with the Manifest changes. 

### Updating the PC Manifests

The manifests are how we replace / add all the needed things to build Android-x86 based iso images. 
So starting from vendor/android-generic/manifests/android_pc, make a copy of one of the vendor folders that is closest to your current vendor setup
ex: (Bliss, Lineage, Tipsy, Tesla) and rename the folder to your vendor name. 

Then in a terminal type:

	$ . build/envsetup.sh
	$ build-x86 -s

Things should start to sync, and you should expect failures at this point. 
If a duplicate is found, edit the 01remove.xml to include or uncomment that remove:
	
	$ nano vendor/android-generic/manifests/android_pc/__YourVendorName__/01remove.xml
	

(Warning: After builds start to get going, you might run innto errors where something from the manifest is still needed. 
Please refer back to this step for that solution)

If a project is found missing, edit the 05other.xml to add that project. 

	$ nano vendor/android-generic/manifests/android_pc/__YourVendorName__/05other.xml

### Using Manifest Patches

Sometimes when you find that a rom has applied something that is just not working right, and a reset is the only option, we have a solution.
In the pc_vendor_patches folder, we look for patches in manifest_patches/.repo/manifests/ before the sync command and apply that. 

These patch files need to be created by making your changes, and then adding them to a single commit. Then generate the patch file: 
	
	$ git format-patch -1
	
After that is made, copy it into your vendor_patches folder after you've created the "manifest_patches" folder. 

#### Example:

vendor/android-generic/patches/google_diff/pc_vendor_patches/aosp/manifest_patches/.repo/manifests/manifest.patch:

	'''	
	From a7d866f9bba852ea17af471c73ceec8303b783c3 Mon Sep 17 00:00:00 2001
	From: Jon West <electrikjesus@gmail.com>
	Date: Fri, 14 Aug 2020 21:13:50 -0400
	Subject: [PATCH] Specify revision for vold

	Change-Id: I6614e893869dc29e92ce3464d49b1aa43c541307
	---
	 snippets/pixel.xml | 2 +-
	 1 file changed, 1 insertion(+), 1 deletion(-)

	diff --git a/snippets/pixel.xml b/snippets/pixel.xml
	index 4d4b62a..9462ba5 100644
	--- a/snippets/pixel.xml
	+++ b/snippets/pixel.xml
	@@ -181,6 +181,6 @@
	   <project path="system/sepolicy" name="system_sepolicy" remote="pixel" />
	   <project path="system/timezone" name="system_timezone" remote="pixel" />
	   <project path="system/update_engine" name="system_update_engine" remote="pixel" />
	-  <project path="system/vold" name="system_vold" remote="pixel" />
	+  <project path="system/vold" name="system_vold" remote="pixel" revision="ee29b67f7490ef0f68e2a167680381d997db3255"/>
	 
	 </manifest>
	--  
	2.17.1
	
	'''

### Updating the Treble Scripts

This step is added automatically in the -n | --vendor-setup part of the script
but to manually add your vendor to the build system. 

    $ nano vendor/android-generic/gsi_roms.lst

Then add your vendor name to the bottom of the list: 

    bliss
    lineage
    __YourVendorName__

Then we scroll down a little more and add our ROMs desired filename to the script:

	# PHH-Treble Vendor Definition
	# UPDATE HERE WHEN PHH SUPPORTED VENDOR IS USED
	if [ -d $rompath/vendor/bliss/ ]; then
		rom="bliss";
		product_name="BLESS-";
	elif [ -d $rompath/vendor/lineage/ ]; then
		rom="lineage";
		product_name="LineageOS-";
	else
		product_name="Android-Generic";
	fi

After that, we can get on with the Manifest changes. 

### Updating the Treble Manifests

The manifests are how we replace / add all the needed things to build Android-x86 based iso images. 
So starting from vendor/android-generic/manifests/android_pc, make a copy of one of the vendor folders that is closest to your current vendor setup
ex: (Bliss, Lineage, Tipsy, Tesla) and rename the folder to your vendor name. 

Then in a terminal type:

	$ . build/envsetup.sh
	$ build-treble -s

Things should start to sync, and you should expect failures at this point. 
If a duplicate is found, edit the 01remove.xml to include or uncomment that remove:
	
	$ nano vendor/android-generic/manifests/treble/__YourVendorName__/01remove.xml
	

(Warning: After builds start to get going, you might run innto errors where something from the manifest is still needed. 
Please refer back to this step for that solution)

If a project is found missing, edit the 05other.xml to add that project. 

	$ nano vendor/android-generic/manifests/treble/__YourVendorName__/05other.xml

### Resolving Patch Conflicts

For the patches, after the first Manifest steps are done, you will want to then make a copy of all the results of the patch scripts. 
Each of the patches applied either resulted in "Applying", "Already applied", or "Conflicts". The only ones we want to pay attention to here are the "Conflicts", 
but only half of them. 

Some of the patches have duplicates for different vendor setups. So you will sometimes get results that look like this:

	Conflicts          system/core/0015-Bliss-init-don-t-bail-out-even-no-SELinux-domain-defined.patch
    Applying          system/core/0015-Tesla-init-don-t-bail-out-even-no-SELinux-domain-defined.patch 0
    
Notice how the one starting with "0015-Bliss-" failed, but the patch starting with "0015-Tesla-" applied correctly? If that happend on your vendor setup, you can ignore that patch. 
But if you only see one patch that had a "Conflicts", that will need to be applied and fixed. 

First, you apply the patch manually:

	$ git am "__patchLocationHere__"
	
You can expect that to fail, but it's crutial to the next step. Next you patch the file again, but using the "patch" command

	$ patch -p1 < "__patchLocationHere__"
	
This will generate the .orig & .rej files to help narrow down what you need to fix. Once the conflicting parts (what's in the .rej files) are resolved, delete the .rej & .orig files, then stage the files:

	$ git add -A

Then we can continue the staged git am commit from the first step:

	$ git am --continue

After applying the patch(s) to your local project folder, remember to generate the patch 
needed to resolve that conflict:

	$ git format-patch -1
	
Then copy the patch to the appropriate vendor folder for the conflict. Any patches that are needed to be done to the ROM before applying the generic set of patches will go to the prepatch folder (pc_vendor_prepatches/treble_vendor_prepatches), while any conflicts that happened from the generic patches themselves should go to the vendor_patches folder (pv_vendor_patches/treble_vendor_patches) Example:

	$ cp system/core/0001-init-don-t-bail-out-even-no-SELinux-domain-defined.patch vendor/android-generic/patches/google_diff/x86/pc_vendor_patches/__YourVendorName__/patches/system/core/0015-init-don-t-bail-out-even-no-SELinux-domain-defined.patch


# Adding Custom Vendor Patchesets
-------------------

In each of the (pc/treble)_vendor_patches/__VendorName__ folders, there is a Patches folder. And in some, you will see a custom_patches folder. If you do not see one there, you can create it. 
In that folder, also create or edit the customizations.lst file and add your customizations name, this will override the filename too, and is not case sensitive. For example, in the vendor/android-generic/patches/google_diff/treble_vendor_patches/bliss/custom_patches/ we have added the bless set of custom patches. and added the word "bless" to the customizations.lst file. The foldername and name you add to the .lst file must be exactly the same. 

Once you freate the folder, go ahead and add your patches there, following the example set by the "bless" set. Then when you use either the --sync or --patch options, the patcher will ask you if you want to apply the customization:

    Searching for vendor patches...

    Found bless customizations!!

    Do you want to use the bless customizations? (This will also rename the build from bliss to bless): [y/n] 

This will ask for each customization found also, allowing you to apply multiple customization patchsets if available. 


# Updating the Script Repo
-------------------

Next step is to add your vendor changes to the script repo for everyone else to be able to benefit from it.
Please submit your changes as a pull request to https://gitlab.com/android-generic/vendor_android-generic 

	cd vendor/android-generic
	git add -A
	git commit -a

Then type in the rom info using the example below as a template please:

	[ROM][ROM Name] Add initial support for ROM Name

	Vendor Prepatches:
	0001-this-patchset.patch
	0002-this-patchset.patch
	
	Vendor Patches:
	0001-this-patchset.patch
	0002-this-patchset.patch
	0003-this-patchset.patch
	0004-this-patchset.patch

From here, we create a pull request:

	git push -o merge_request.create

Or if that isn't working, you can try:

	git push --set-upstream origin q10 -o merge_request.create
	

# Credits

We'd like to say thanks to all these great individuals first:
@phhusson @cwhuang @maurossi @goffioul @me176c-dev @bosconovic @farmerbb @aclegg2011 @eternityson and many others

And these great teams second:
@Google @LineageOS @GZR @OmniROM @SlimROM @ParanoidAndroid and many others, for you still lead the way for Open Innovation in the Android community. 
Android is a trademark of Google LLC.

