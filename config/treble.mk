my_path := $(call my-dir)

LOCAL_PATH := $(my_path)
include $(CLEAR_VARS)

# Inherit comon android-generic
$(call inherit-product,vendor/android-generic/config/common.mk)

# Inherit vendors
$(call inherit-product,vendor/android-generic/config/vendors.mk)

# ROM specific overlays
PRODUCT_ENFORCE_RRO_EXCLUDED_OVERLAYS += $(VENDOR_PATH)/patches/google_diff/treble_vendor_patches/$(CURRENT_ROM)/overlay
DEVICE_PACKAGE_OVERLAYS += $(VENDOR_PATH)/patches/google_diff/treble_vendor_patches/$(CURRENT_ROM)/overlay/common

# Required packages
PRODUCT_PACKAGES += \
    LatinIME

# Include Bliss GSI overlays
PRODUCT_PACKAGE_OVERLAYS += vendor/android-generic/overlay/gsi

PRODUCT_SHIPPING_API_LEVEL := 19

PRODUCT_COPY_FILES += \
    frameworks/native/data/etc/android.software.activities_on_secondary_displays.xml:system/etc/permissions/android.software.activities_on_secondary_displays.xml \
    frameworks/native/data/etc/android.software.midi.xml:system/etc/permissions/android.software.midi.xml \
    frameworks/native/data/etc/android.software.picture_in_picture.xml:system/etc/permissions/android.software.picture_in_picture.xml \
    frameworks/native/data/etc/android.software.print.xml:system/etc/permissions/android.software.print.xml \
    frameworks/native/data/etc/android.software.webview.xml:system/etc/permissions/android.software.webview.xml \
    frameworks/native/data/etc/android.hardware.gamepad.xml:system/etc/permissions/android.hardware.gamepad.xml \

# Optional packages
PRODUCT_PACKAGES += \
    LiveWallpapersPicker \
    PhotoTable \
    Terminal

# Custom Lineage packages
PRODUCT_PACKAGES += \
    htop \
    nano 

# Set Bliss Desktop Mode by default
# Use 'export BLISS_DESKTOPMODE=true' or set 
# 'BLISS_DESKTOPMODE := true' within BoardConfig.mk. 
ifeq ($(BLISS_DESKTOPMODE),true)

# Enable MultiWindow
PRODUCT_PROPERTY_OVERRIDES += \
    persist.sys.debug.multi_window=true
    persist.sys.debug.desktop_mode=true
    
endif

# Exchange support
PRODUCT_PACKAGES += \
    Exchange2 \

# !!EXPERIMENTAL!!
# QEMU-based native bridge for Android-x86 - https://github.com/goffioul/ax86-nb-qemu
ifeq ($(USE_X86LIBNB),true)

# Remove packages
PRODUCT_PACKAGES += \
    libnb-qemu \
    libnb-qemu-guest

endif

